module.exports = function (grunt) {
    
    // Project configuration.
    grunt.initConfig({
        
        pkg: grunt.file.readJSON('package.json'),
        app: {
            app: 'app',
            dist: 'dist',
            baseurl: ''
        },
        jekyll: {
            files:[
            '<%= app.app %>/**/*.{html,yml,md,mkd,markdown}'],
            tasks:[ 'jekyll:server']
        }
    });
    
    // Load the plugin that provides the "uglify" task.
    
    grunt.loadNpmTasks('grunt-jekyll');
    
    // Default task(s).
    grunt.registerTask('default',[ 'jekyll']);
};